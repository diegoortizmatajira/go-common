package gorilla

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/diegoortizmatajira/go-common/abstraction"
	"gitlab.com/diegoortizmatajira/go-common/base"
	"gitlab.com/diegoortizmatajira/go-common/details/presentation/rest"
)

// BaseServer defines standard operations for an App server
type BaseServer struct {
	*base.Component
	settings *rest.Settings
}

// ServerHandlerRegistar describes an method for Service Handlers initialization
type ServerHandlerRegistar interface {
	InitializeServiceHandlers(r *mux.Router) error
}

// ServerInfrastructureInitializer describes an method for infrastructure initialization
type ServerInfrastructureInitializer interface {
	InitializeInfrastructure() error
}

// NewBaseServer creates a new Application Server
func NewBaseServer(logger abstraction.Logger) *BaseServer {
	return &BaseServer{
		base.NewServerComponent(logger, "rest"),
		rest.DefaultSettings(),
	}
}

// Setup allows to configure the server settings
func (s BaseServer) Setup(builder func(*rest.Settings)) {
	if builder != nil {
		builder(s.settings)
	}
}

// Run executes the Server using the provided server definition
func (s BaseServer) Run(srvDefinition interface{}) {

	r := mux.NewRouter()

	if initializer, ok := srvDefinition.(ServerInfrastructureInitializer); ok {
		if err := initializer.InitializeInfrastructure(); err != nil {
			s.Logger.WithError(err).Error("Error setting up infrastructure")
			return
		}
	}

	if registrar, ok := srvDefinition.(ServerHandlerRegistar); ok {
		if err := registrar.InitializeServiceHandlers(r); err != nil {
			s.Logger.WithError(err).Error("Error setting up service handlers")
			return
		}
	}

	s.Logger.WithField("Middleware", "CORS").Info("Enabling Middleware")
	r.Use(mux.CORSMethodMiddleware(r))
	s.Logger.WithField("Middleware", "Logging").Info("Enabling Middleware")
	r.Use(rest.NewLoggingMiddleware(s.Logger))

	httpServer := &http.Server{
		Addr:         s.settings.Address,
		Handler:      r,
		ReadTimeout:  s.settings.ReadTimeout,
		WriteTimeout: s.settings.WriteTimeout,
	}

	// Run the server
	s.Logger.WithField("Address", s.settings.Address).Info("Starting server listener")
	if err := httpServer.ListenAndServe(); err != nil {
		switch err {
		case http.ErrServerClosed:
			s.Logger.Info("Server listener stopped")
			return
		default:
			s.Logger.WithError(err).Error("Error running the server")
			return
		}
	}
}
