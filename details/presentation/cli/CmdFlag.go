package cli

import (
	"fmt"
	"strings"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// FlagType describes the data type of a flag
type FlagType int

const (
	// StringFlag for a flag with string value
	StringFlag FlagType = iota
	// IntFlag for a flag with a int value
	IntFlag
	// BoolFlag for a flag with a boolean value
	BoolFlag
	// DurationFlag for a flag with a duration value
	DurationFlag
)

// CmdFlag describes a flag setting
type CmdFlag struct {
	Name         string
	Type         FlagType
	DefaultValue interface{}
	Usage        string
	ConfigKey    string
}

// UsageString returns the usage string including the environment variable name
func (f CmdFlag) UsageString() string {
	env := strings.Replace(strings.ToUpper(f.KeyString()), ".", "_", -1)
	return fmt.Sprintf("%s [$%s]", f.Usage, env)
}

// KeyString returns the Config Key provided or translated from the flag name replacing '-' with '.'
func (f CmdFlag) KeyString() string {
	if f.ConfigKey != "" {
		return f.ConfigKey
	}
	return strings.Replace(strings.ToLower(f.Name), "-", ".", -1)
}

// ApplyToCommand adds the flag to a command
func (f CmdFlag) ApplyToCommand(cmd *cobra.Command) {
	switch f.Type {
	case StringFlag:
		cmd.Flags().String(f.Name, f.DefaultValue.(string), f.UsageString())
		break
	case IntFlag:
		cmd.Flags().Int(f.Name, f.DefaultValue.(int), f.UsageString())
		break
	case BoolFlag:
		cmd.Flags().Bool(f.Name, f.DefaultValue.(bool), f.UsageString())
		break
	case DurationFlag:
		cmd.Flags().Duration(f.Name, f.DefaultValue.(time.Duration), f.UsageString())
		break
	}
	viper.BindPFlag(f.KeyString(), cmd.Flag(f.Name))
	viper.SetDefault(f.KeyString(), f.DefaultValue)
}
