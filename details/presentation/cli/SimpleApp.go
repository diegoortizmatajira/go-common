package cli

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/viper"
)

// SimpleApp ṕrovides a single command CLI
type SimpleApp struct {
	settings CmdSettings
}

// NewSimpleApp returns a new instance of a simple application
func NewSimpleApp(settings CmdSettings) *SimpleApp {
	return &SimpleApp{
		settings,
	}
}

// Run prepares and executes the application
func (a SimpleApp) Run() {
	rootCmd := a.settings.GetCommand()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
