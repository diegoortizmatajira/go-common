package cli

import "github.com/spf13/cobra"

// CmdSettings provides the command settings for the CLI
type CmdSettings struct {
	UseHint          string
	ShortDescription string
	LongDescription  string
	Setup            func(args []string)
	Run              func(args []string)
	Flags            []CmdFlag
}

// GetCommand returns a command with the settings
func (s CmdSettings) GetCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use:   s.UseHint,
		Long:  s.LongDescription,
		Short: s.ShortDescription,
		PreRun: func(cmd *cobra.Command, args []string) {
			if s.Setup != nil {
				s.Setup(args)
			}
		},
		Run: func(cmd *cobra.Command, args []string) {
			if s.Run != nil {
				s.Run(args)
			}
		},
	}
	for _, flag := range s.Flags {
		flag.ApplyToCommand(cmd)
	}
	return cmd
}
