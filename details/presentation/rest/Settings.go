package rest

import "time"

// Settings contains the configuration values for the AppServer
type Settings struct {
	Address         string
	ReadTimeout     time.Duration
	WriteTimeout    time.Duration
	GracefulTimeout time.Duration
}

// DefaultSettings provides the default settings for a server
func DefaultSettings() *Settings {
	return &Settings{
		Address:         "[::]:9080",
		ReadTimeout:     5 * time.Second,
		WriteTimeout:    10 * time.Second,
		GracefulTimeout: 15 * time.Second,
	}
}
