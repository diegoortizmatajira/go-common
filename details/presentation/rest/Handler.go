package rest

import (
	"encoding/json"
	"io"
	"net/http"

	"gitlab.com/diegoortizmatajira/go-common/abstraction"
	"gitlab.com/diegoortizmatajira/go-common/base"
)

// Handler represents a basic REST handler
type Handler struct {
	*base.Component
}

// NewHandler returns a new basic REST handler instance
func NewHandler(logger abstraction.Logger, handlerName string) *Handler {
	return &Handler{
		base.NewHandlerComponent(logger, "rest", handlerName),
	}
}

//DecodeFromJSON obtains a value from the body of the request
func (h Handler) DecodeFromJSON(r io.Reader, value interface{}) error {
	dec := json.NewDecoder(r)
	return dec.Decode(value)
}

// RespondWithJSON sends a response in RespondWithJSON Format
func (h Handler) RespondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

// RespondWithError sends an error response in JSON Format
func (h Handler) RespondWithError(w http.ResponseWriter, code int, message string) {
	h.RespondWithJSON(w, code, map[string]string{"error": message})
}
