package rest

import (
	"net/http"

	"gitlab.com/diegoortizmatajira/go-common/abstraction"
)

// NewLoggingMiddleware returns a middleware for logging requests
func NewLoggingMiddleware(logger abstraction.Logger) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			logger.
				WithField("URI", r.RequestURI).
				Info("Processing request")
			next.ServeHTTP(w, r)
		})
	}
}
