package micro

import (
	"gitlab.com/diegoortizmatajira/go-common/abstraction"
	"gitlab.com/diegoortizmatajira/go-common/details/logging/apexlog"

	"github.com/micro/cli/v2"
	"github.com/micro/go-micro/v2"
	"github.com/micro/go-micro/v2/config"
	"github.com/micro/go-micro/v2/config/source"
	cliCfg "github.com/micro/go-micro/v2/config/source/cli"
	"github.com/micro/go-micro/v2/logger"
	"github.com/micro/go-plugins/logger/apex/v2"
)

// AppServer defines standard operations for an App server
type AppServer struct {
	Logger abstraction.Logger
}

// AppServerOptionsInitializer describes an method for service options initialization
type AppServerOptionsInitializer interface {
	InitializeOptions() []micro.Option
}

// AppServerFlagsInitializer describes an method for flag initialization
type AppServerFlagsInitializer interface {
	InitializeFlags() []cli.Flag
}

// AppServerInfrastructureInitializer describes an method for infrastructure initialization
type AppServerInfrastructureInitializer interface {
	InitializeInfrastructure() error
}

// AppServerHandlerRegistar describes an method for Service Handlers initialization
type AppServerHandlerRegistar interface {
	InitializeServiceHandlers(service micro.Service) error
}

func (s *AppServer) initService(appDefinition interface{}) micro.Service {
	var opts []micro.Option
	if initializer, ok := appDefinition.(AppServerOptionsInitializer); ok {
		newOpts := initializer.InitializeOptions()
		logger.DefaultLogger.Logf(logger.InfoLevel, "Micro service options added: %v new options", len(newOpts))
		opts = append(opts, newOpts...)
	}
	if initializer, ok := appDefinition.(AppServerFlagsInitializer); ok {
		flags := initializer.InitializeFlags()
		logger.DefaultLogger.Logf(logger.InfoLevel, "Micro flags added: %v new flags", len(flags))
		opts = append(opts, micro.Flags(flags...))
	}
	// Create a new service using the configuration settings
	service := micro.NewService(opts...)
	var flagConfigSource source.Source
	service.Init(
		// Loads CLI configuration
		micro.Action(getSettingsFromFlags(&flagConfigSource)),
	)
	config.Load(
		flagConfigSource,
		//env.NewSource(),
	)
	s.applyLoggingSettings()
	return service
}

func (s *AppServer) applyLoggingSettings() {
	//Sets log level from config
	logLevel, _ := logger.GetLevel(config.DefaultConfig.Get("log", "level").String("info"))
	//Sets log format from config
	var formatterOption logger.Option
	if logFormat := config.Get("log", "format").String("text"); logFormat == "json" {
		formatterOption = apex.WithJSONHandler()
	} else {
		formatterOption = apex.WithTextHandler()
	}
	//Sets the apex logger as default
	logger.DefaultLogger = apex.New(
		apex.WithLevel(logLevel),
		formatterOption,
	)
	s.Logger = apexlog.NewDefaultLogger()
}

// Run executes the Server using the provided app definition
func (s *AppServer) Run(appDefinition interface{}) error {
	// Initialize the service
	service := s.initService(appDefinition)
	if initializer, ok := appDefinition.(AppServerInfrastructureInitializer); ok {
		if err := initializer.InitializeInfrastructure(); err != nil {
			s.Logger.WithError(err).Error("Error setting up infrastructure")
			return err
		}
	}
	if registrar, ok := appDefinition.(AppServerHandlerRegistar); ok {
		if err := registrar.InitializeServiceHandlers(service); err != nil {
			s.Logger.WithError(err).Error("Error setting up service handlers")
			return err
		}
	}
	// Run the server
	if err := service.Run(); err != nil {
		s.Logger.WithError(err).Error("Error running the service")
		return err
	}
	return nil
}

func getSettingsFromFlags(cliSrc *source.Source) func(c *cli.Context) error {
	return func(c *cli.Context) error {
		*cliSrc = cliCfg.NewSource(
			cliCfg.Context(c),
		)
		return nil
	}
}
