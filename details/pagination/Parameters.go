package pagination

// Parameters provides the required parameters to use pagination
type Parameters struct {
	PageSize   int
	PageNumber int
}
