package db

import "database/sql"

type directDBResolver struct {
	db *sql.DB
}

func (r directDBResolver) GetDB() *sql.DB {
	return r.db
}

// NewDirectDBResolver returns a new database resolver
func NewDirectDBResolver(db *sql.DB) Resolver {
	return &directDBResolver{db: db}
}
