package db

import "database/sql"

// Resolver describes an object responsible of deliver a database connection
type Resolver interface {
	GetDB() *sql.DB
}

// DefaultDBResolverBuilder describes an object responsible of setting the resolver configuration
type DefaultDBResolverBuilder interface {
	SetDefaultDB(*sql.DB)
}
