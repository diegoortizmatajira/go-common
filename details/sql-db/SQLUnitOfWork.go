package db

import (
	"context"
	"database/sql"
	"errors"

	"gitlab.com/diegoortizmatajira/go-common/abstraction"
)

// UnitOfWork describes an SQL based unit of work
type UnitOfWork interface {
	abstraction.UnitOfWork
	GetExecutor() ContextExecutor
}

type sqlUnitOfWork struct {
	resolver Resolver
	tx       *sql.Tx
}

func (uow *sqlUnitOfWork) Begin(ctx context.Context) error {
	db := uow.resolver.GetDB()
	if uow.tx == nil {
		tx, err := db.BeginTx(ctx, nil)
		if err != nil {
			uow.tx = nil
			return err
		}
		uow.tx = tx
	}
	return nil
}

func (uow *sqlUnitOfWork) Complete() error {
	if uow.tx == nil {
		return errors.New("There is no transaction to complete")
	}
	err := uow.tx.Commit()
	uow.tx = nil
	return err
}

func (uow *sqlUnitOfWork) internalRollback(checkOpenTransaction bool) error {
	if checkOpenTransaction && uow.tx == nil {
		return errors.New("There is no transaction to complete")
	}
	if uow.tx != nil {
		err := uow.tx.Rollback()
		uow.tx = nil
		return err
	}
	return nil
}

func (uow *sqlUnitOfWork) Rollback() error {
	return uow.internalRollback(true)
}

func (uow *sqlUnitOfWork) DeferredRollback() {
	uow.internalRollback(false)
}

func (uow *sqlUnitOfWork) GetExecutor() ContextExecutor {
	if uow.tx != nil {
		return uow.tx
	}
	return uow.resolver.GetDB()
}

// NewSQLUnitOfWork creates a new Unit of work
func NewSQLUnitOfWork(resolver Resolver) UnitOfWork {
	return &sqlUnitOfWork{
		resolver: resolver,
		tx:       nil,
	}
}
