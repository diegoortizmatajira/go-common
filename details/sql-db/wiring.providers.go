package db

import (
	"github.com/google/wire"
	"gitlab.com/diegoortizmatajira/go-common/abstraction"
)

// WiringProviders provides the basic dependencies
var WiringProviders = wire.NewSet(
	wire.Bind(new(abstraction.UnitOfWork), new(UnitOfWork)),
	NewSQLUnitOfWork,
)

// SingleDBWiringProviders provides the dependencies for a single DB scenario
var SingleDBWiringProviders = wire.NewSet(
	NewSingleDBResolver,
	NewSingleDBResolverBuilder,
)

// DirectDBWiringProviders provides the dependencies for a direct provided DB
var DirectDBWiringProviders = wire.NewSet(
	NewDirectDBResolver,
)
