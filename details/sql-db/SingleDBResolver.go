package db

import "database/sql"

var singleDBConnection *sql.DB

type singleDBResolver struct {
}

func (r singleDBResolver) GetDB() *sql.DB {
	return singleDBConnection
}

func (r singleDBResolver) SetDefaultDB(db *sql.DB) {
	singleDBConnection = db
}

// NewSingleDBResolver returns a new single database resolver
func NewSingleDBResolver() Resolver {
	return &singleDBResolver{}
}

// NewSingleDBResolverBuilder returns a new single database resolver builder
func NewSingleDBResolverBuilder() DefaultDBResolverBuilder {
	return &singleDBResolver{}
}
