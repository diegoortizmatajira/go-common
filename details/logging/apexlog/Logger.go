package apexlog

import (
	"github.com/apex/log"
	"github.com/apex/log/handlers/discard"
	"github.com/apex/log/handlers/json"
	"github.com/apex/log/handlers/text"
	"gitlab.com/diegoortizmatajira/go-common/abstraction"
)

type logger struct {
	log.Interface
}

func (l *logger) Trace(msg string) abstraction.BasicLogger {
	return newLogger(l.Interface.Trace(msg))
}

func (l *logger) WithFields(fields abstraction.FieldSet) abstraction.Logger {
	var fs = make(log.Fields)
	for k, v := range fields {
		fs[k] = v
	}
	return newLogger(l.Interface.WithFields(fs))
}

func (l *logger) WithField(key string, value interface{}) abstraction.Logger {
	return newLogger(l.Interface.WithField(key, value))
}

func (l *logger) WithError(err error) abstraction.BasicLogger {
	return newLogger(l.Interface.WithError(err))
}

func (l *logger) Setup(format string, level string) {
	var h log.Handler = text.Default
	switch format {
	case abstraction.JsonLogFormat:
		h = json.Default
	case abstraction.DiscardLogFormat:
		h = discard.Default
	}
	log.SetHandler(h)
	log.SetLevel(convertToApexLevel(level))
}

func convertToApexLevel(level string) log.Level {
	switch level {
	case abstraction.DebugLevel:
		return log.DebugLevel
	case abstraction.InfoLevel:
		return log.InfoLevel
	case abstraction.WarnLevel:
		return log.WarnLevel
	case abstraction.ErrorLevel:
		return log.ErrorLevel
	case abstraction.FatalLevel:
		return log.FatalLevel
	default:
		return log.InfoLevel
	}
}

func newLogger(o log.Interface) abstraction.Logger {
	return &logger{
		o,
	}
}

// NewDefaultLogger provides an instance of a logger with the default instance of apex log
func NewDefaultLogger() abstraction.Logger {
	return newLogger(log.Log)
}

// NewLogBuilder returns a Log config facility
func NewLogBuilder() abstraction.LogBuilder {
	return &logger{}
}
