package apexlog

import "github.com/google/wire"

// DependencyInjectionSet provides the basic dependencies
var WiringProviders = wire.NewSet(
	NewDefaultLogger,
	NewLogBuilder,
)
