package base

import (
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gofrs/uuid"
)

// HasID defines if an struct has ID
type HasID interface {
	GetID() uuid.UUID
}

// Entity provides common elements for all entities
type Entity struct {
	ID        uuid.UUID
	CreatedAt time.Time
	UpdatedAt time.Time `validate:"gtefield=CreatedAt"`
}

// GetID obtains the ID of the Entity
func (e *Entity) GetID() uuid.UUID {
	return e.ID
}

// Validate ensures the data in a Location is valid
func (e *Entity) Validate() error {
	v := validator.New()
	return v.Struct(e)
}

// GenerateNewID generate and assigns a new ID to the entity
func (e *Entity) GenerateNewID() error {
	newID, err := uuid.NewV4()
	if err != nil {
		return err
	}
	e.ID = newID
	return nil
}

// ResetID generate and assigns a new ID to the entity
func (e *Entity) ResetID() {
	e.ID = uuid.Nil
}

// NewEntity creates a new instance of base entity
func NewEntity() Entity {
	return Entity{
		uuid.Nil,
		time.Time{},
		time.Time{},
	}
}

// LoadEntity loads data into a new instance of base entity
func LoadEntity(id uuid.UUID, createdAt time.Time, updatedAt time.Time) Entity {
	return Entity{
		id,
		createdAt,
		updatedAt,
	}
}
