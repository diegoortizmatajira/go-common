package base

import (
	"fmt"

	"gitlab.com/diegoortizmatajira/go-common/abstraction"
	"gitlab.com/diegoortizmatajira/go-common/naming"
)

// Component provides a base functionality for multiple components
type Component struct {
	componentName string
	Logger        abstraction.Logger
}

func newComponent(logger abstraction.Logger, componentName string) *Component {
	return &Component{
		componentName,
		logger.WithField("ComponentName", componentName),
	}
}

// NewServerComponent creates a new base component for a presentation server
func NewServerComponent(logger abstraction.Logger, presentationType string) *Component {
	return newComponent(logger, naming.GetServerID(presentationType))
}

// NewHandlerComponent creates a new base component for a presentation handler
func NewHandlerComponent(logger abstraction.Logger, presentationType string, handlerName string) *Component {
	return newComponent(logger, naming.GetHandlerID(presentationType, handlerName))
}

// NewServiceComponent creates a new base component for a service
func NewServiceComponent(logger abstraction.Logger, serviceName string) *Component {
	return newComponent(logger, naming.GetServiceID(serviceName))
}

// NewRepositoryComponent creates a new base component for a repository
func NewRepositoryComponent(logger abstraction.Logger, storageType string, repositoryName string) *Component {
	return newComponent(logger, naming.GetRepositoryID(storageType, repositoryName))
}

// NewError creates a new error instance with the provided information
func (c *Component) NewError(message string, err error) error {
	return fmt.Errorf("%s (Component: %s)\n%w", message, c.componentName, err)
}
