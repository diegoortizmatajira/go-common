package base_test

import (
	"testing"
	"time"

	"gitlab.com/diegoortizmatajira/go-common/base"

	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/suite"
)

type baseEntitySuite struct {
	suite.Suite
}

func (s baseEntitySuite) TestGenerateNewId() {
	entityValue := base.NewEntity()
	originalID := entityValue.ID
	err := entityValue.GenerateNewID()
	newID := entityValue.ID
	s.NoError(err)
	s.NotEqual(originalID, newID)
}

func (s baseEntitySuite) TestValidations() {
	validID, _ := uuid.NewV4()
	dateValue1 := time.Now().Add(-1 * time.Hour)
	dateValue2 := dateValue1.Add(15 * time.Minute)
	testCases := []struct {
		name  string
		data  *base.Entity
		valid bool
	}{
		{
			name:  "New Record",
			data:  base.NewEntity(),
			valid: true,
		},
		{
			name:  "Record with correct dates",
			data:  base.LoadEntity(validID, dateValue1, dateValue2),
			valid: true,
		},
		{
			name:  "Record with invalid dates",
			data:  base.LoadEntity(validID, dateValue2, dateValue1),
			valid: false,
		},
	}
	for _, tC := range testCases {
		s.Run(tC.name, func() {
			if tC.valid {
				s.NoError(tC.data.Validate())
			} else {
				s.Error(tC.data.Validate())
			}
		})
	}
}

func TestBaseEntitySuite(t *testing.T) {
	suite.Run(t, new(baseEntitySuite))
}
