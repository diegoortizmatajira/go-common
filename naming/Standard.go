package naming

import "fmt"

// EntityTemplate provides standard naming for logging
var EntityTemplate = "dom.%s"

// RepositoryTemplate provides standard naming for logging
var RepositoryTemplate = "repo.%s.%s"

// ServiceTemplate provides standard naming for logging
var ServiceTemplate = "svc.%s"

// HandlerTemplate provides standard naming for logging
var HandlerTemplate = "api.%s.%s"

// ServerTemplate provides standard naming for logging
var ServerTemplate = "api.%s.server"

// GetEntityID returns the complete identifier for an Entity
func GetEntityID(name string) string {
	return fmt.Sprintf(EntityTemplate, name)
}

// GetRepositoryID returns the complete identifier for a Repository
func GetRepositoryID(storageType string, name string) string {
	return fmt.Sprintf(RepositoryTemplate, storageType, name)
}

// GetServiceID returns the complete identifier for a Repository
func GetServiceID(name string) string {
	return fmt.Sprintf(ServiceTemplate, name)
}

// GetHandlerID returns the complete identifier for a Presentation Handler
func GetHandlerID(presentationType string, name string) string {
	return fmt.Sprintf(HandlerTemplate, presentationType, name)
}

// GetServerID returns the complete identifier for a Presentation Server
func GetServerID(presentationType string) string {
	return fmt.Sprintf(ServerTemplate, presentationType)
}
