//go:generate mockgen -source=$GOFILE -destination=../mocks/abstraction_Logger.go -package=mocks

package abstraction

const (
	TextLogFormat    string = "text"
	JsonLogFormat    string = "json"
	DiscardLogFormat string = "discard"

	DebugLevel string = "debug"
	InfoLevel  string = "info"
	WarnLevel  string = "warn"
	ErrorLevel string = "error"
	FatalLevel string = "fatal"
)

// FieldSet represents a map of entry level data used for structured logging.
type FieldSet map[string]interface{}

// BasicLogger represents a logger with basic capabilities
type BasicLogger interface {
	Debug(msg string)
	Info(msg string)
	Warn(msg string)
	Error(msg string)
	Fatal(msg string)
	Debugf(msg string, v ...interface{})
	Infof(msg string, v ...interface{})
	Warnf(msg string, v ...interface{})
	Errorf(msg string, v ...interface{})
	Fatalf(msg string, v ...interface{})
	Trace(msg string) BasicLogger
}

// Logger represents a logger
type Logger interface {
	BasicLogger
	WithFields(fields FieldSet) Logger
	WithField(key string, value interface{}) Logger
	WithError(err error) BasicLogger
}

// LogBuilder represents a Log config facility
type LogBuilder interface {
	Setup(format string, level string)
}
