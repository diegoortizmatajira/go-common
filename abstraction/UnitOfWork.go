//go:generate mockgen -source=$GOFILE -destination=../mocks/abstraction_UnitOfWork.go -package=mocks

package abstraction

import "context"

// UnitOfWork defines the abstraction of the Transaction concept
type UnitOfWork interface {
	Begin(ctx context.Context) error
	Complete() error
	Rollback() error
	DeferredRollback()
}
