module gitlab.com/diegoortizmatajira/go-common

go 1.15

require (
	github.com/apex/log v1.9.0
	github.com/go-playground/validator/v10 v10.3.0
	github.com/gofrs/uuid v3.3.0+incompatible
	github.com/golang/mock v1.3.1
	github.com/google/wire v0.4.0
	github.com/gorilla/mux v1.8.0
	github.com/micro/cli/v2 v2.1.2
	github.com/micro/go-micro/v2 v2.9.1
	github.com/micro/go-plugins/logger/apex/v2 v2.9.1
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.6.1
)
