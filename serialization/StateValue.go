package serialization

import "encoding/json"

// StateValue stores a snapshot of an object
type StateValue struct {
	value []byte
}

// String returns the string representation of the state value.
func (s *StateValue) String() string {
	return string(s.value[:])
}

// NewStateValue creates a new state value from an object
func NewStateValue(value interface{}) (*StateValue, error) {
	JSONvalue, err := json.Marshal(value)
	if err != nil {
		return nil, err
	}
	return &StateValue{
		JSONvalue,
	}, nil
}
